/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Collection;
import java.util.HashSet;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 *
 * @author peter
 */
@Entity
@Table(name="SPARCLUB")
public class Sparclub implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)    
    @Column(name="spcID")
    private Long spcID;
    @Column(name="spcName")
    private String spcName;
    @Column(name="boxCount")
    private int boxCount;
    @Column(name="penitentFee")
    private double penitentFee;
    @Column(name="claimedAmount")
    private double claimedAmount;
    @Column(name="clearanceDay")
    private String clearanceDay;
    @Column(name="clearanceTime")
    private Time clearanceTime;
    @Column(name="accountIBAN")
    private String accountIBAN;
    @Column(name="musicPath")
    private String musicPath;
    @Column(name="backupPath")
    private String backupPath;
    @Column(name="providerSMSUser")
    private String providerSMSUser;
    @Column(name="providerSMSPwd")
    private String providerSMSPwd;
    @Column(name="chargeSMS")
    private double chargeSMS;
    @Column(name="adminEmail")
    private String adminEmail;
    @Column(name="shutdownCommand")
    private String shutdownCommand;

    //@ElementCollection(fetch = FetchType.LAZY)
    //@CollectionTable(name="SALE_ITEM", joinColumns = @JoinColumn(name="spcID"))
    //@MapKeyColumn(name="itemID")
    //@Column(name="saleItems")
    //private Collection<SaleItem> saleItems;
    

    public Sparclub() {
    }
    
    public Sparclub(String theName) {
        spcName = theName;
    }

    @OneToMany(cascade=ALL, mappedBy="spcID")
    public Collection<SaleItem> getSaleItems() {
        return new HashSet();
    }
    
 
    @OneToMany(cascade=ALL, mappedBy = "spcID")
    public Collection<ConsolidatedClearance> getConsolidatedClearances() {
        return new HashSet();
    }
    
    @OneToMany(cascade=ALL, mappedBy = "spcID")
    public Collection<SpcMember> getSpcMembers() {
        return new HashSet();
    }
    
    
    @OneToMany(cascade=ALL, mappedBy = "spcID")
    public Collection<Person> getPersons() {
        return new HashSet();
    }
    
    
    
    public Long getId() {
        return spcID;
    }

    public void setId(Long id) {
        this.spcID = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (spcID != null ? spcID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sparclub)) {
            return false;
        }
        Sparclub other = (Sparclub) object;
        if ((this.spcID == null && other.spcID != null) || (this.spcID != null && !this.spcID.equals(other.spcID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.myservice4u.mavenfx.entity.Sparclub[ id=" + spcID + " ]";
    }

    /**
     * @return the spcName
     */
    public String getSpcName() {
        return spcName;
    }

    /**
     * @param spcName the spcName to set
     */
    public void setSpcName(String spcName) {
        this.spcName = spcName;
    }

    /**
     * @return the penitentFee
     */
    public double getPenitentFee() {
        return penitentFee;
    }

    /**
     * @param penitentFee the penitentFee to set
     */
    public void setPenitentFee(double penitentFee) {
        this.penitentFee = penitentFee;
    }

    /**
     * @return the claimedAmount
     */
    public double getClaimedAmount() {
        return claimedAmount;
    }

    /**
     * @param claimedAmount the claimedAmount to set
     */
    public void setClaimedAmount(double claimedAmount) {
        this.claimedAmount = claimedAmount;
    }

    /**
     * @return the clearanceDay
     */
    public String getClearanceDay() {
        return clearanceDay;
    }

    /**
     * @param clearanceDay the clearanceDay to set
     */
    public void setClearanceDay(String clearanceDay) {
        this.clearanceDay = clearanceDay;
    }

    /**
     * @return the clearanceTime
     */
    public Time getClearanceTime() {
        return clearanceTime;
    }

    /**
     * @param clearanceTime the clearanceTime to set
     */
    public void setClearanceTime(Time clearanceTime) {
        this.clearanceTime = clearanceTime;
    }

    /**
     * @return the accountIBAN
     */
    public String getAccountIBAN() {
        return accountIBAN;
    }

    /**
     * @param accountIBAN the accountIBAN to set
     */
    public void setAccountIBAN(String accountIBAN) {
        this.accountIBAN = accountIBAN;
    }

    /**
     * @return the musicPath
     */
    public String getMusicPath() {
        return musicPath;
    }

    /**
     * @param musicPath the musicPath to set
     */
    public void setMusicPath(String musicPath) {
        this.musicPath = musicPath;
    }

    /**
     * @return the backupPath
     */
    public String getBackupPath() {
        return backupPath;
    }

    /**
     * @param backupPath the backupPath to set
     */
    public void setBackupPath(String backupPath) {
        this.backupPath = backupPath;
    }

    /**
     * @return the providerSMSUser
     */
    public String getProviderSMSUser() {
        return providerSMSUser;
    }

    /**
     * @param providerSMSUser the providerSMSUser to set
     */
    public void setProviderSMSUser(String providerSMSUser) {
        this.providerSMSUser = providerSMSUser;
    }

    /**
     * @return the providerSMSPwd
     */
    public String getProviderSMSPwd() {
        return providerSMSPwd;
    }

    /**
     * @param providerSMSPwd the providerSMSPwd to set
     */
    public void setProviderSMSPwd(String providerSMSPwd) {
        this.providerSMSPwd = providerSMSPwd;
    }

    /**
     * @return the chargeSMS
     */
    public double getChargeSMS() {
        return chargeSMS;
    }

    /**
     * @param chargeSMS the chargeSMS to set
     */
    public void setChargeSMS(double chargeSMS) {
        this.chargeSMS = chargeSMS;
    }

    /**
     * @return the adminEmail
     */
    public String getAdminEmail() {
        return adminEmail;
    }

    /**
     * @param adminEmail the adminEmail to set
     */
    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    /**
     * @return the shutdownCommand
     */
    public String getShutdownCommand() {
        return shutdownCommand;
    }

    /**
     * @param shutdownCommand the shutdownCommand to set
     */
    public void setShutdownCommand(String shutdownCommand) {
        this.shutdownCommand = shutdownCommand;
    }

    /**
     * @return the boxCount
     */
    public int getBoxCount() {
        return boxCount;
    }

    /**
     * @param boxCount the boxCount to set
     */
    public void setBoxCount(int boxCount) {
        this.boxCount = boxCount;
    }
    
}
