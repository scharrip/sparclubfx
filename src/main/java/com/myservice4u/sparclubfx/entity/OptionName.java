/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author peter
 */
@Entity
@Table(name="OPTION_NAME")
public class OptionName implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="optionKey")
    private Long optionKey;
    @Column(name="optionLabel")
    private String optionLabel;

    public OptionName(){
        
    }

    public Long getOptionKey() {
        return optionKey;
    }

    public void setOptionKey(Long optionKey) {
        this.optionKey = optionKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (optionKey != null ? optionKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the optionKey fields are not set
        if (!(object instanceof OptionName)) {
            return false;
        }
        OptionName other = (OptionName) object;
        if ((this.optionKey == null && other.optionKey != null) || (this.optionKey != null && !this.optionKey.equals(other.optionKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.myservice4u.mavenfx.entity.OptionName[ id=" + optionKey + " ]";
    }
    
}
