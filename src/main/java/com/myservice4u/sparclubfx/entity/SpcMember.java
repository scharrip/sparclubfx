/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author peter
 */
@Entity
@Table(name="MEMBER")
public class SpcMember implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="memberID")
    private Long memberID;
    @Column(name="spcID")
    private Long spcID;
    @Column(name="personID")
    private Long personID;
    @Column(name="boxNumber")
    private Long boxNumber;
    @Column(name="entryDate")
    private Date entryDate;
    @Column(name="exitDate")
    private Date exitDate;

    public SpcMember(){
        
    }
    
    @ManyToOne
    @JoinColumn(name="spcID", nullable=false)
    public Sparclub getSparclub() {
        return new Sparclub();
    }
    
    public void setSparclub (Sparclub theSparclub) {
        //
    }
    
    @ManyToOne
    @JoinColumn(name = "personID")
    @JoinColumns({
        @JoinColumn(name="spcID", referencedColumnName="spcID"),
        @JoinColumn(name="personID", referencedColumnName="personID")})
    public Person getPerson() {
        return new Person();
    }

    public void setPerson (Person thePerson) {
        //
    }
    
    @OneToMany
    @JoinColumn(name = "memberID")
    public Collection<Clearance> getClearances() {
        return new HashSet();
    }
    
    public void setClearances(Collection<Clearance> slearances) {
        //;
    }
    
    @Id
    public Long getMemberID() {
        return memberID;
    }

    public void setMemberID(Long memberID) {
        this.memberID = memberID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getMemberID() != null ? getMemberID().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the memberID fields are not set
        if (!(object instanceof SpcMember)) {
            return false;
        }
        SpcMember other = (SpcMember) object;
        if ((this.getMemberID() == null && other.getMemberID() != null) || (this.getMemberID() != null && !this.memberID.equals(other.memberID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.myservice4u.mavenfx.entity.Member[ id=" + getMemberID() + " ]";
    }


    /**
     * @param boxNumber the boxNumber to set
     */
    public void setBoxNumber(Long boxNumber) {
        this.boxNumber = boxNumber;
    }

    /**
     * @return the entryDate
     */
    public Date getEntryDate() {
        return entryDate;
    }

    /**
     * @param entryDate the entryDate to set
     */
    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    /**
     * @return the exitDate
     */
    public Date getExitDate() {
        return exitDate;
    }

    /**
     * @param exitDate the exitDate to set
     */
    public void setExitDate(Date exitDate) {
        this.exitDate = exitDate;
    }

    /**
     * @return the boxNumber
     */
    public Long getBoxNumber() {
        return boxNumber;
    }
    
}
