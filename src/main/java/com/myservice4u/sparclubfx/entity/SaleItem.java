/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author peter
 */
@Entity
@Table(name="SALE_ITEM")
public class SaleItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="itemID")
    private Long itemID;
    @Column(name="spcID")
    private Long spcID;
    @Column(name="itemName")
    private String itemName;
    @Column(name="itemPicture")
    private String itemPicture;
    @Column(name="itemAmount")
    private double itemAmount;
    @Column(name="itemAvailable")
    private boolean itemAvailable ;
    
    public SaleItem(){
    }
    
    
    @ManyToOne
    @JoinColumn(name="spcID", nullable=false)
    public Sparclub getSparclub() {
        return new Sparclub();
    }

    
    public Long getItemID() {
        return itemID;
    }

    public void setItemID(Long itemID) {
        this.itemID = itemID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemID != null ? itemID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the itemID fields are not set
        if (!(object instanceof SaleItem)) {
            return false;
        }
        SaleItem other = (SaleItem) object;
        if ((this.itemID == null && other.itemID != null) || (this.itemID != null && !this.itemID.equals(other.itemID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.myservice4u.mavenfx.entity.SaleItem[ id=" + itemID + " ]";
    }
    
}
