/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author peter
 */
@Entity
@Table(name="PERSON")
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="personID")
    private Long personID;
    @Column(name="spcID")
    private Long spcID;
    @Column(name="nicName")
    private String nicName;
    @Column(name="firstName")
    private String firstName;
    @Column(name="surName")
    private String surName;
    @Column(name="accountBalance")
    private double accountBalance;
    @Column(name="referenceBalance")
    private double referenceBalance;
    @Column(name="addressStreet")
    private String addressStreet;
    @Column(name="addressCity")
    private String addressCity;
    @Column(name="addressCode")
    private String addressCode;
    @Column(name="dateOfBirth")
    private Date dateOfBirth;
    @Column(name="cellPhone")
    private String cellPhone;
    @Column(name="eMailAddress")
    private String eMailAddress;
    @Column(name="amountSMS")
    private double amountSMS;
    @Column(name="balanceUpdateDate")
    private Date balanceUpdateDate;
    @Column(name="balanceUpdateTime")
    private Time balanceUpdateTime;

    public Person(){
        
    }

    @ManyToOne
    @JoinColumn(name="spcID", nullable=false)
    public Sparclub getSparclub() {
        return new Sparclub();
    }
    
    public void setSparclub (Sparclub theSparclub) {
        //
    }
    

    @OneToMany(mappedBy = "personID")
    public Collection<SpcMember> getSpcMembers() {
        return new HashSet();
    }
    
    public Long getPersonID() {
        return personID;
    }

    public void setPersonID(Long personID) {
        this.personID = personID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personID != null ? personID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the personID fields are not set
        if (!(object instanceof Person)) {
            return false;
        }
        Person other = (Person) object;
        if ((this.personID == null && other.personID != null) || (this.personID != null && !this.personID.equals(other.personID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.myservice4u.mavenfx.entity.Person[ id=" + personID + " ]";
    }

    /**
     * @return the spcID
     */
    public Long getSpcID() {
        return spcID;
    }

    /**
     * @param spcID the spcID to set
     */
    public void setSpcID(Long spcID) {
        this.spcID = spcID;
    }

    /**
     * @return the nicName
     */
    public String getNicName() {
        return nicName;
    }

    /**
     * @param nicName the nicName to set
     */
    public void setNicName(String nicName) {
        this.nicName = nicName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the surName
     */
    public String getSurName() {
        return surName;
    }

    /**
     * @param surName the surName to set
     */
    public void setSurName(String surName) {
        this.surName = surName;
    }

    /**
     * @return the accountBalance
     */
    public double getAccountBalance() {
        return accountBalance;
    }

    /**
     * @param accountBalance the accountBalance to set
     */
    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    /**
     * @return the referenceBalance
     */
    public double getReferenceBalance() {
        return referenceBalance;
    }

    /**
     * @param referenceBalance the referenceBalance to set
     */
    public void setReferenceBalance(double referenceBalance) {
        this.referenceBalance = referenceBalance;
    }

    /**
     * @return the addressStreet
     */
    public String getAddressStreet() {
        return addressStreet;
    }

    /**
     * @param addressStreet the addressStreet to set
     */
    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    /**
     * @return the addressCity
     */
    public String getAddressCity() {
        return addressCity;
    }

    /**
     * @param addressCity the addressCity to set
     */
    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    /**
     * @return the addressCode
     */
    public String getAddressCode() {
        return addressCode;
    }

    /**
     * @param addressCode the addressCode to set
     */
    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the cellPhone
     */
    public String getCellPhone() {
        return cellPhone;
    }

    /**
     * @param cellPhone the cellPhone to set
     */
    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    /**
     * @return the eMailAddress
     */
    public String geteMailAddress() {
        return eMailAddress;
    }

    /**
     * @param eMailAddress the eMailAddress to set
     */
    public void seteMailAddress(String eMailAddress) {
        this.eMailAddress = eMailAddress;
    }

    /**
     * @return the amountSMS
     */
    public double getAmountSMS() {
        return amountSMS;
    }

    /**
     * @param amountSMS the amountSMS to set
     */
    public void setAmountSMS(double amountSMS) {
        this.amountSMS = amountSMS;
    }

    /**
     * @return the balanceUpdateDate
     */
    public Date getBalanceUpdateDate() {
        return balanceUpdateDate;
    }

    /**
     * @param balanceUpdateDate the balanceUpdateDate to set
     */
    public void setBalanceUpdateDate(Date balanceUpdateDate) {
        this.balanceUpdateDate = balanceUpdateDate;
    }

    /**
     * @return the balanceUpdateTime
     */
    public Time getBalanceUpdateTime() {
        return balanceUpdateTime;
    }

    /**
     * @param balanceUpdateTime the balanceUpdateTime to set
     */
    public void setBalanceUpdateTime(Time balanceUpdateTime) {
        this.balanceUpdateTime = balanceUpdateTime;
    }
    
}
