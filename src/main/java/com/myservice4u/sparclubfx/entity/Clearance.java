/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author peter
 */
@Entity
@Table(name="CLEARANCE")
public class Clearance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="clearanceID")
    private Long clearanceID;
    @Column(name="spcID")
    private Long spcID;
    @Column(name="boxNumber")
    private Long boxNumber;
    @Column(name="memberID")
    private Long memberID;
    @Column(name="clearanceAmount")
    private double clearanceAmount;
    @Column(name="clearanceDay")
    private Date clearanceDay;

    //Hallo
    public Clearance(){
        
    }
    
    @ManyToOne
    @JoinColumn(name = "memberID")
    public SpcMember getSpcMember() {
        return new SpcMember();
    }
    
    public void setSpcMember (SpcMember theSpcMember) {
        //
    }
    
    public Long getClearanceID() {
        return clearanceID;
    }

    public void setClearanceID(Long clearanceID) {
        this.clearanceID = clearanceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clearanceID != null ? clearanceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the clearanceID fields are not set
        if (!(object instanceof Clearance)) {
            return false;
        }
        Clearance other = (Clearance) object;
        if ((this.clearanceID == null && other.clearanceID != null) || (this.clearanceID != null && !this.clearanceID.equals(other.clearanceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.myservice4u.mavenfx.entity.Clearance[ id=" + clearanceID + " ]";
    }
    
}
