/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author peter
 */
@Entity
@Table(name="CONSOLIDATED_CLEARANCE")
public class ConsolidatedClearance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="consolidatedId")
    private Long consolidatedId;
    @Column(name="spcID")
    private Long spcID;
    @Column(name="clearanceDay")
    private Date clearanceDay;
    @Column(name="clearanceAmount")
    private double clearanceAmount;

    public ConsolidatedClearance(){
        
    }
    
    @ManyToOne
    @JoinColumn(name="spcID", nullable=false)
    public Sparclub getSparclub() {
        return new Sparclub();
    }
    
    public void setSparclub (Sparclub theSparclub) {
        //
    }
    
    public Long getConsolidatedId() {
        return consolidatedId;
    }

    public void setConsolidatedId(Long consolidatedId) {
        this.consolidatedId = consolidatedId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getConsolidatedId() != null ? getConsolidatedId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the consolidatedId fields are not set
        if (!(object instanceof ConsolidatedClearance)) {
            return false;
        }
        ConsolidatedClearance other = (ConsolidatedClearance) object;
        if ((this.getConsolidatedId() == null && other.getConsolidatedId() != null) || (this.getConsolidatedId() != null && !this.consolidatedId.equals(other.consolidatedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.myservice4u.mavenfx.entity.ConClear[ id=" + getConsolidatedId() + " ]";
    }

    /**
     * @return the spcID
     */
    public Long getSpcID() {
        return spcID;
    }

    /**
     * @param spcID the spcID to set
     */
    public void setSpcID(Long spcID) {
        this.spcID = spcID;
    }

    /**
     * @return the clearanceDay
     */
    public Date getClearanceDay() {
        return clearanceDay;
    }

    /**
     * @param clearanceDay the clearanceDay to set
     */
    public void setClearanceDay(Date clearanceDay) {
        this.clearanceDay = clearanceDay;
    }

    /**
     * @return the clearanceAmount
     */
    public double getClearanceAmount() {
        return clearanceAmount;
    }

    /**
     * @param clearanceAmount the clearanceAmount to set
     */
    public void setClearanceAmount(double clearanceAmount) {
        this.clearanceAmount = clearanceAmount;
    }
    
}
