/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.migration;
import java.util.Calendar;

/**
 *
 * @author Peter
 */
public class SpcCalendar {
    int myYear, myMonth, myDay, myHour;
    
    public static SpcCalendar getToday(){
        return new SpcCalendar(Calendar.getInstance());
    }
    public SpcCalendar(int year, int month, int day){
        myYear = year;
        myMonth = month;
        myDay = day;
        myHour = 0;
    }
    public SpcCalendar(int year, int month, int day, int hour){
        myYear = year;
        myMonth = month;
        myDay = day;
        myHour = hour;
    }
    public SpcCalendar(Calendar theDate){
        myYear = theDate.get(Calendar.YEAR);
        myMonth = theDate.get(Calendar.MONTH) + 1;
        myDay = theDate.get(Calendar.DAY_OF_MONTH);
        myHour = theDate.get(Calendar.HOUR_OF_DAY);
    }
    public SpcCalendar(String theDate){
        if (!(theDate == null) && !theDate.isEmpty()){
            SpcCalendar tempCalendar = transformString(theDate);
            myYear = tempCalendar.myYear;
            myMonth = tempCalendar.myMonth;
            myDay = tempCalendar.myDay;
            myHour = 0;
        }
    }
    
    public Calendar getDate(){
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(myYear, myMonth - 1, myDay, myHour, 0);
        return myCalendar;
    }
    
    public int getDay(){
        return myDay;
    }
    public int getMonth(){
        return myMonth;
    }
    public int getYear(){
        return myYear;
    }
    public int getHour()
    {
        return myHour;
    }
    public Calendar getNextHour(){
        Calendar newDate = getDate();
        newDate.roll(Calendar.HOUR_OF_DAY, true);
        return newDate;
    }
    
    public void setDay(int day){
        myDay = day;
    }
    public void setMonth(int month){
        myMonth = month;
    }
    public void setYear(int year){
        myYear = year;
    }
    public void setHour(int hour){
        myHour = hour;
    }
    public int compareTo(SpcCalendar otherDate){
        int retVal = 0;
        if(otherDate.getYear() == myYear){
            if(otherDate.getMonth() == myMonth){
                if(otherDate.getDay() == myDay){
                    retVal = 0;
                }else if(otherDate.getDay() < myDay){
                    retVal = 1;
                }else if(otherDate.getDay() > myDay){
                    retVal = -1;
                }
            }else if(otherDate.getMonth() < myMonth){
                retVal = 1;
            }else if(otherDate.getMonth() > myMonth){
                retVal = -1;
            }
        }else if(otherDate.getYear() < myYear){ 
            retVal = 1;
        }else if(otherDate.getYear() > myYear){
            retVal = -1;
        }
        return retVal;
    }
    public int compareTo(Calendar otherDate){
        return compareTo(new SpcCalendar(otherDate.get(Calendar.YEAR), otherDate.get(Calendar.MONTH) + 1, otherDate.get(Calendar.DAY_OF_YEAR)));
    }
    public int compareTo(String otherDate){
        return compareTo(transformString(otherDate));
    }
    private SpcCalendar transformString(String aDate){
        String theDate = aDate.replace(".", "#").replace("-", "#");
        String [] theDateAsString = theDate.split("#");
        
        SpcCalendar resultCalendar = null;
        if(Integer.parseInt(theDateAsString[0]) > 31){
            resultCalendar = new SpcCalendar(Integer.parseInt(theDateAsString[0]), Integer.parseInt(theDateAsString[1]), Integer.parseInt(theDateAsString[2])); 
        }else if(Integer.parseInt(theDateAsString[2]) > 31){
            resultCalendar = new SpcCalendar(Integer.parseInt(theDateAsString[2]), Integer.parseInt(theDateAsString[1]), Integer.parseInt(theDateAsString[0])); 
        }
        
        return resultCalendar;
    }
    public static String getFormattedTime(Calendar dateTime){
        String time = "";
        int hour = dateTime.get(Calendar.HOUR_OF_DAY);
        int min = dateTime.get(Calendar.MINUTE);
        int sec = dateTime.get(Calendar.SECOND);
        
        if(hour < 10){
            time = "0" + hour + ":";
        }else{
            time = hour + ":";
        }
        if(min < 10){
            time = time + "0" + min + ":";
        }else{
            time = time + min + ":";
        }
        
        if(sec < 10){
            time = time + "0" + sec;
        }else{
            time = time + sec;
        }
       
        return time;
    }
    
    public String getDateAsString(){
        String theDate;
        if(myDay < 10){
            theDate = "0" + myDay + ".";
        }else{
            theDate = myDay + ".";
        }
        
        if(myMonth < 10){
            theDate += "0" + myMonth + ".";
        }else{
            theDate += myMonth + ".";
        }
        
        return theDate + myYear;
    }
    
    public String getSQLDateAsString(){
        String theDate;
        
        if(myMonth < 10){
            theDate = "0" + myMonth + "-";
        }else{
            theDate = myMonth + "-";
        }
        
        if(myDay < 10){
            theDate += "0" + myDay;
        }else{
            theDate += myDay;
        }
        
        return myYear + "-" + theDate;
    }

    public SpcCalendar addWeek(int weeks){
        Calendar oldDate = this.getDate();
        oldDate.set(Calendar.WEEK_OF_YEAR, oldDate.get(Calendar.WEEK_OF_YEAR) + weeks);
        return new SpcCalendar(oldDate);
    }
}
