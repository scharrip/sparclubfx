/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myservice4u.sparclubfx.migration;

import java.util.Timer;
import java.util.TimerTask;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

import java.io.FileOutputStream;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Peter
 */
public class SpcXMLManagerX {
    //private static final Logger LOG = Logger.getLogger(Sparclub.class.getName());
    public final static String ELEMENT_DISPLAY_NAME = "DisplayName";
    public final static String SPARCLUB_ELEMENT = "Sparclub";
    public final static String CLEARANCE_SUM_ELEMENT = "DoneClearances";
    public final static String BOX_ELEMENT = "Box";
    public final static String CLEARANCE_ELEMENT = "Clearance";
    public final static String MEMBER_ELEMENT = "Member";
    public final static String MEMBER_ELEMENT_CLEARENCES = "Clearances";

    
    private static final String IMPORT_FILE_NAME = "Sparclub";
    private static final String IMPORT_FILE_POSTFIX = ".xml";
    private org.w3c.dom.Document aktDocument;
    private int backupCounter;
    private Timer safeTimer;
    /**
     *
     */
    public SpcXMLManagerX(){

        //SAXBuilder builder = new SAXBuilder(); 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        File xmlFile = new File(IMPORT_FILE_NAME + IMPORT_FILE_POSTFIX);
        aktDocument = null;
        safeTimer = new Timer(true);
        
        if(xmlFile.exists()){
            try{
                // Create a DOM-building parser
                DocumentBuilder builder = factory.newDocumentBuilder();

                // Parse and return the content of the XML file into a Document
                // instance.
                aktDocument = builder.parse(xmlFile);
                //aktDocument = builder.build(xmlFile);
            }catch(Exception e){
                //LOG.log(Level.SEVERE,"Fehler beim lesen des XML Documents", e.toString());
            }    
        }else{
            //LOG.log(Level.WARNING, "XML Import: Keine Datei gefunden");
        }
    }


    
    public org.w3c.dom.Document getXMLDocument(){
        return aktDocument;
    }  
    
    public void saveDocument(){
        //LOG.log(Level.INFO, "");
        saveAt("", IMPORT_FILE_NAME + IMPORT_FILE_POSTFIX);
    }
    private void saveAt(String pathName, String fileName){
        //LOG.log(Level.INFO, "Path: {0} File: {1}", new Object[]{pathName, fileName});
        //XMLOutputter out = new XMLOutputter();
        TransformerFactory tFactory = TransformerFactory.newInstance();
        
        if(!pathName.equals("")){
            pathName = pathName + "/";
        }
        
        try{
            Transformer transformer = tFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            
            Source s = new DOMSource(aktDocument);
            Result res = new StreamResult( new FileOutputStream(pathName + fileName));
            transformer.transform(s, res);
            //out.output(aktDocument, new FileOutputStream(pathName + fileName));
        }catch(Exception e){
            //LOG.log(Level.SEVERE, "XML Export fehlgeschlagen");
        }  
    }
    private void backup(final String thePath){
        Thread backupThread = new Thread(){
            @Override
            public void run(){
                saveDocument();
                File backupPath = new File(thePath);
                String backupFile = IMPORT_FILE_NAME + "_Backup_" + SpcCalendar.getToday().getDateAsString() + "_" + backupCounter++ + IMPORT_FILE_POSTFIX;
                File theNewBackupFile;;
                if(backupPath.isDirectory()){
                     theNewBackupFile = new File(backupPath, backupFile);
                     while(theNewBackupFile.exists()){
                         backupFile = IMPORT_FILE_NAME + "_Backup_" + SpcCalendar.getToday().getDateAsString() + "_" + backupCounter++ + IMPORT_FILE_POSTFIX;
                         theNewBackupFile = new File(backupPath, backupFile);
                     }
                    saveAt(backupPath.getPath(), backupFile);
                }
            }
        };
        backupThread.start();
    }
    
    private void finalBackup(final String thePath){
        
        saveDocument();
        File backupPath = new File(thePath);
        String backupFile = IMPORT_FILE_NAME + "_Backup_" + SpcCalendar.getToday().getDateAsString() + "_" + backupCounter++ + IMPORT_FILE_POSTFIX;
        File theNewBackupFile;;
        if(backupPath.isDirectory()){
             theNewBackupFile = new File(backupPath, backupFile);
             while(theNewBackupFile.exists()){
                 backupFile = IMPORT_FILE_NAME + "_Backup_" + SpcCalendar.getToday().getDateAsString() + "_" + backupCounter++ + IMPORT_FILE_POSTFIX;
                 theNewBackupFile = new File(backupPath, backupFile);
             }
        saveAt(backupPath.getPath(), backupFile);
        }
    }
    
    
    
    public Document getDocument(){
        //LOG.log(Level.INFO, "");
        return aktDocument;
    }
}
